# week06

## Getting started

First part of the assigment in "exe_1" branch.
Second part of the assigment in "exe_2" branch.

## EXE_1

In order to run the project, just execute ng serve.

## EXE_2

In order to run the project, before execute ng serve, you need to run npm install. This project use a country-state-city: https://www.npmjs.com/package/country-state-city
